//
//  ArticleViewController.m
//  Wikipedia
//
//  Created by Paolo Ramos on 11/24/17.
//  Copyright © 2017 PaoloLabs. All rights reserved.
//

#import "ArticleViewController.h"

@interface ArticleViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation ArticleViewController
@synthesize webView;
@synthesize activityIndicatorView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.delegate = self;
    NSString *urlString = @"https://en.wikipedia.org/wiki/Special:Random";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    self.activityIndicatorView.hidden = NO;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    self.activityIndicatorView.hidden = YES;
}


@end
