//
//  ViewController.swift
//  Random Quote Machine
//
//  Created by Paolo Ramos on 11/24/17.
//  Copyright © 2017 PaoloLabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var quotes: [Quote] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let quote = quotes[indexPath.row]
        
        if let quote = quote.value {
            cell.textLabel?.text = quote
        }
        
        return cell
    }
    
    func getData() {
        do {
            quotes = try context.fetch(Quote.fetchRequest())
        }
        catch {
            print("Fetching Failed")
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let quote = quotes[indexPath.row]
            context.delete(quote)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do {
                quotes = try context.fetch(Quote.fetchRequest())
            }
            catch {
                print("Fetching Failed")
            }
        }
        tableView.reloadData()
    }
    
    @IBAction func showRandomQuote(_ sender: UIBarButtonItem) {
        if !quotes.isEmpty {
            let quote = quotes[randomNumber(inRange: 0...quotes.count-1)]
            let text = quote.value ?? ""
            let alert = UIAlertController(title: "Quote", message: text, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

