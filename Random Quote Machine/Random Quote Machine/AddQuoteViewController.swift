//
//  AddQuoteViewController.swift
//  Random Quote Machine
//
//  Created by Paolo Ramos on 11/24/17.
//  Copyright © 2017 PaoloLabs. All rights reserved.
//

import UIKit
import CoreData

class AddQuoteViewController: UIViewController {
    
    @IBOutlet weak var taskTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        if var text = taskTextField.text {
            text = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if !text.isEmpty {
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                let predicate = NSPredicate(format: "value == %@", text)
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Quote")
                fetchRequest.predicate = predicate
                
                do {
                    let results = try context.fetch(fetchRequest) as! [NSManagedObject]
                    if results.isEmpty {
                        let quote = Quote(context: context)
                        quote.value = text
                        // Save the data to coredata
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }
                } catch {
                    print(error)
                }
            }
            let _ = navigationController?.popViewController(animated: true)
        }
    }
    
}
