//
//  ViewController.swift
//  Twitch TV Swift
//
//  Created by Paolo Ramos on 11/24/17.
//  Copyright © 2017 PaoloLabs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    let idSelectTableViewCell = "UserDetailTableViewCell"
    var arrayUsers = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "\(self.idSelectTableViewCell)", bundle: nil), forCellReuseIdentifier: "\(self.idSelectTableViewCell)")
        Service().getUserList { (success, response) in
            if success {
                print(response)
                self.arrayUsers = response["featured"].arrayValue
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayUsers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(self.idSelectTableViewCell)", for: indexPath) as! UserDetailTableViewCell
        let data = self.arrayUsers[indexPath.row]
        let imageFromURL = data["image"].stringValue
        if !imageFromURL.isEmpty {
            cell.userImageView.downloadedFrom(link: imageFromURL)
        }
        else {
            cell.userImageView.image = UIImage()
        }
        cell.userNameLabel.text = data["title"].stringValue
        cell.lastSyncLabel.text = "Stream: \(data["stream"]["stream_type"].stringValue)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}

