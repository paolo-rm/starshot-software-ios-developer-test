//
//  Service.swift
//  Twitch TV Swift
//
//  Created by Paolo Ramos on 11/24/17.
//  Copyright © 2017 PaoloLabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class Service: NSObject {

    public func getUserList(_ completionHandler:@escaping (_ success: Bool, _ responseObject: JSON) -> ()) {
        _ = Alamofire.request("https://wind-bow.glitch.me/twitch-api/streams/featured").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result

            if let json = response.result.value {
//                print("JSON: \(json)") // serialized json response
                completionHandler(true, JSON(json))
            }
            else {
                completionHandler(false, JSON(NSData()))
            }
        }
    }
}
